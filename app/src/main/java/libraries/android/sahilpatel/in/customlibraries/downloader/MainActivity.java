package libraries.android.sahilpatel.in.customlibraries.downloader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import libraries.android.sahilpatel.in.customlibraries.R;
import libraries.android.sahilpatel.in.customlibraries.services.DownloadService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button_download_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDownloadService();
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();

                if(bundle != null) {
                    String string = bundle.getString(DownloadService.FILEPATH);
                    int resultCode = bundle.getInt(DownloadService.RESULT);

                    if (resultCode == RESULT_OK) {
                        Toast.makeText(MainActivity.this, "Download done", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onReceive: "+string);
                    }

                    if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(MainActivity.this, "Download failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
    }


    private void startDownloadService() {

        Intent intent = new Intent(this,DownloadService.class);
        intent.putExtra(DownloadService.FILENAME,"anotherDog.jpg");
        intent.putExtra(DownloadService.URL,"http://sahilpatel.in/BulletinBoard/uploads/");
        startService(intent);
        Log.d(TAG, "startDownloadService: "+"Service started");
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver,new IntentFilter(DownloadService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }



}
