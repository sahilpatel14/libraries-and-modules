package libraries.android.sahilpatel.in.customlibraries;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class CachingDemo extends AppCompatActivity {

    private LruCache<String, Bitmap> mMemoryCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caching_demo);

        performCachingMagic();

    }

    private void performCachingMagic() {

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {

            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {

        if(getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key,bitmap);
        }
    }


    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }


    private void loadBitmap(int resId, ImageView imageView) {
        final String imageKey = String.valueOf(resId);

        final Bitmap bitmap = getBitmapFromMemCache(imageKey);

        if(bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
        else {
            // TODO: 9/27/2016 Some sort of default image 
            imageView.setImageBitmap(null);

            // TODO: 9/27/2016 Call a service that would fetch the image for you. 
        }
    }

}
